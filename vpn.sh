#!/bin/bash
starttime=`date +%s`
echo 'Install community.general collection from ansible-galaxy'
ansible-galaxy collection install community.general
echo 'Install community.crypto collection from ansible-galaxy'
ansible-galaxy collection install community.crypto
ansible-playbook -i vpn deploy_vpn.yml
finishtime=`date +%s`
printf 'Seconds:'
echo $finishtime-$starttime | bc
