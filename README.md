Tested with Ansible >= 2.12.9, Ubuntu 22.04

Put your server IP or FQDN in file "./vpn".

Run tasks with ./vpn.sh.

VPN software for installation can be added/removed by editing "./deploy_vpn.yml"

Role "preinst" is mandatory.
